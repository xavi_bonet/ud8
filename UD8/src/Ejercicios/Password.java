package Ejercicios;

import java.util.Random;

public class Password {
	
	// ATRIBUTOS
	private int longitud;
	private String contraseña;
	
	private final char defaultLongitud = 8;
	
	
	// CONSTRUCTORES
    public Password() {
        this.longitud = defaultLongitud;
        this.contraseña = generarContraseña(longitud); 
    }
	
    public Password(int longitud) {
        this.longitud = longitud;
        this.contraseña = generarContraseña(longitud); 
    }
    
    
    // GETTERS
    public int getLongitud() {
		return longitud;
	}
    
	public String getContraseña() {
		return contraseña;
	}
    
    
    //SETTERS
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	
	// METODOS
    private static String generarContraseña(int longitud) {
        String asciiMayuscula = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String asciiMinuscula = asciiMayuscula.toLowerCase();
        String digitos = "1234567890";
        String asciiChars = asciiMayuscula + asciiMinuscula + digitos;
        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;
        Random random = new Random();
        while (i < longitud) {
        	stringBuilder.append(asciiChars.charAt(random.nextInt(asciiChars.length())));
            i++;
        }
        return stringBuilder.toString();
    }
}
