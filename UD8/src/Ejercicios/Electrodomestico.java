package Ejercicios;

public class Electrodomestico {

	// ATRIBUTOS
	protected double precioBase;
	protected String color;
	protected char consumoEnergetico;
	protected double peso;
	
	private final double defaultPrecioBase = 100;
	private final String defaultColor = "blanco";
	private final char defaultConsumoEnergetico = 'F';
	private final double defaultPeso = 5;
	
	
	// CONSTRUCTORES
	public Electrodomestico() {
		this.precioBase = defaultPrecioBase;
		this.color = comprovarColor(defaultColor);
		this.consumoEnergetico = defaultConsumoEnergetico;
		this.peso = defaultPeso; 
	}
	
	public Electrodomestico(double precioBase, double peso) {
		this.precioBase = precioBase;
		this.color = comprovarColor(defaultColor);
		this.consumoEnergetico = defaultConsumoEnergetico;
		this.peso = peso; 
	}
	
	public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
		this.precioBase = precioBase;
		this.color = comprovarColor(color);
		this.consumoEnergetico = consumoEnergetico;
		this.peso = peso; 
	}

	
	// GETTERS
	public double getPrecioBase() {
		return precioBase;
	}
	
	public String getColor() {
		return color;
	}

	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}
	
	public double getPeso() {
		return peso;
	}
	
	
	//SETTERS
	public void setPrecioBase(double precioBase) {
		this.precioBase = precioBase;
	}

	public void setColor(String color) {
		this.color = comprovarColor(color);
	}

	public void setConsumoEnergetico(char consumoEnergetico) {
		this.consumoEnergetico = consumoEnergetico;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	
	// METODOS
	private static String comprovarColor(String color) {
		if (color.toLowerCase() == "blanco" || color.toLowerCase() == "negro" || color.toLowerCase() == "rojo" || color.toLowerCase() == "azul" || color.toLowerCase() == "gris") {
			return color;
		} else {
			return "blanco";
		}
	}
	
}
