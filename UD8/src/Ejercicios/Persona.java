package Ejercicios;

public class Persona {
	
	// ATRIBUTOS
	private String nombre;
	private int edad;
	private String dni;
	private char sexo;
	private double peso;
	private double altura;
	
	private final char defaultSexo = 'H';
	
	
	// CONSTRUCTORES
    public Persona() {
        this.nombre = "";
        this.edad = 0;
        this.dni = null;
        this.sexo = defaultSexo;
        this.peso = 0;
        this.altura = 0;   
    }
    
    public Persona(String nombre, int edad, char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.dni = null;
        this.sexo = sexo;
        this.peso = 0;
        this.altura = 0;  
    }
	
    public Persona(String nombre, int edad, String dni, char sexo, double peso, double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.dni = dni;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;   
    }
	
    
	// GETTERS
    public String getNombre() {
        return this.nombre;
    }
    
    public int getEdad() {
        return this.edad;
    }
    
    public String getDni() {
		return dni;
	}
    
	public char getSexo() {
		return sexo;
	}
	
	public double getPeso() {
		return peso;
	}
	
	public double getAltura() {
		return altura;
	}
	
	
	// SETTERS
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
	
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
	public void setDni(String dni) {
		this.dni = dni;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

}
