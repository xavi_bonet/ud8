package Ejercicios;

public class TestApp {

	public static void main(String[] args) {
		
		// crear objetos de la classe persona
		Persona persona1 = new Persona();
		Persona persona2 = new Persona("Xavi", 21, 'H');
		Persona persona3 = new Persona("Xavi", 21, "47923843P", 'H', 65, 1.70);
		
		// Comprovar classe Password creando 2 objetos y mostrando el contenido
		Password p1 = new Password();
		Password p2 = new Password(15);
		System.out.println("Contraseña: " + p1.getContraseña() + " | Longitud: " + p1.getLongitud());
		System.out.println("Contraseña: " + p2.getContraseña() + " | Longitud: " + p2.getLongitud());

		// crear objetos de la classe Electrodomestico y comprovar metodo
		Electrodomestico e1 = new Electrodomestico();
		Electrodomestico e2 = new Electrodomestico(10, 1);
		Electrodomestico e3 = new Electrodomestico(10, "amarillo", 'F', 1);
		
		System.out.println(e3. getColor());
		
		// crear objetos de la classe serie
		Serie s1 = new Serie();
		Serie s2 = new Serie("Titulo", "Creador");
		Serie s3 = new Serie("Titulo", 5, "Genero", "Creador");
		
	}

}
