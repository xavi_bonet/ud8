package Ejercicios;

public class Serie {

	// ATRIBUTOS
	private String titulo;
	private int numeroDeTemporadas;
	private boolean entregado;
	private String genero;
	private String creador;
	
	final private int defaultNumeroDeTemporadas = 3;
	final private boolean defaultEntregado = false;
	
	
	// CONSTRUCTORES
	public Serie() {
		this.titulo = "";
		this.numeroDeTemporadas = defaultNumeroDeTemporadas;
		this.entregado = defaultEntregado;
		this.genero = "";
		this.creador = "";
	}
	
	public Serie(String titulo, String creador) {
		this.titulo = titulo;
		this.numeroDeTemporadas = defaultNumeroDeTemporadas;
		this.entregado = defaultEntregado;
		this.genero = "";
		this.creador = creador;
	}
	
	public Serie(String titulo, int numeroDeTemporadas, String genero, String creador) {
		this.titulo = titulo;
		this.numeroDeTemporadas = defaultNumeroDeTemporadas;
		this.entregado = defaultEntregado;
		this.genero = genero;
		this.creador = creador;
	}

	
	// GETTERS Y SETTERS
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getNumeroDeTemporadas() {
		return numeroDeTemporadas;
	}

	public void setNumeroDeTemporadas(int numeroDeTemporadas) {
		this.numeroDeTemporadas = numeroDeTemporadas;
	}

	public boolean isEntregado() {
		return entregado;
	}

	public void setEntregado(boolean entregado) {
		this.entregado = entregado;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}
}
