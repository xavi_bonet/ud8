![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)

# UD8 - Java POO (Programación Orientada a Objetos)

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| Xavier Bonet Daga | Master | Project Manager | 01/02/2021 |   |   |  |

#### 2. Description
```
Ejercicios para practicar la programación orientada a objetoss.
```

#### 3. Link a un demo con el proyecto desplegado: https://gitlab.com/xavi_bonet/ud8.git

```
UD8 - programación orientada a objetos / https://gitlab.com/xavi_bonet/ud8.git
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
Java - jdk-8
IDE - Eclipse Enterprise 
```
###### Command line 
```

```

#### 5. Screenshot imagen que indique cómo debe verse el proyecto.
